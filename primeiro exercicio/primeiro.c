/******************************************************************************
Faça um programa que lê três palavras do teclado
e imprime as três palavras na ordem inversa.
*******************************************************************************/

#include <stdio.h>
#include <string.h>

int main()
{
	char palavra1[255];
	char palavra2[255];
	char palavra3[255];
	printf("Insira a primeira palavra\n");
	scanf("%s", palavra1);

	printf("Insira a segunda palavra\n");
	scanf("%s", palavra2);

	printf("Insira a terceira palavra\n");
	scanf("%s", palavra3);

	printf("%s %s %s", palavra3, palavra2, palavra1);
	return 0;
}
